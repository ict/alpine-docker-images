#!/bin/sh
set -eu

BRANCH=$1

if [ "$BRANCH" = 'edge' ]; then
	cat repositories.edge
elif [ "$(apk version -t "$BRANCH" v3.16)" = '<' ]; then  # < 3.16
	esh repositories.old.esh BRANCH=$BRANCH
else  # >= v3.16
	esh repositories.stable.esh BRANCH=$BRANCH
fi
